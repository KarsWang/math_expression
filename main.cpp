#include "math_parser.h"

int main(int argc, char* argv[])
{
	ExpressionCalculater<double> clc;
	
	clc.set_expression(argv[1]);
	int ret = clc.init(true, false);
	if (ret != 0)
	{
		printf("init failed !\n");
		return -1;
	}

	printf("result :%.02f \n", clc.calculate());

	return 0;
}
