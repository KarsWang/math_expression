#ifndef _MATH_PARSER_H_
#define _MATH_PARSER_H_

#if defined(_MSVC_LANG)
#if _MSVC_LANG < 201402L
message("c++ 14 required")
#endif
#elif __cplusplus < 201402L
message("c++ 14 required")
#endif

#include <cmath>
#include <cstdarg>
#include <regex>
#include <set>
#include <functional>
#include <stack>
#include <list>
#include <chrono>
#include <map>

enum CalculateType
{
	_undefined, _number, _function, _left_brackets, _right_brackets, _array
};
typedef CalculateType CType;
typedef std::string string;
typedef const string& cqstr;
typedef std::vector<string> strvec;
typedef std::set<string> stringset;

template <typename ValueType>
inline std::string get_c_type() { return typeid(ValueType).name(); }

constexpr uint32_t cValueBufferSize = 8;
constexpr uint32_t cMaxParamsNum = 11;
constexpr uint32_t cMaxArrayNum = 20;

class SimpleBufferStack
{
public:
	SimpleBufferStack(int element_size = sizeof(double));
	~SimpleBufferStack();

	void push(void* value);
	void pop();
	bool empty() const;
	void* top() const;
	void clear();

private:
	uint32_t _element_size = 1;
	uint32_t max_length = 1024 * 1024;
	void* _buffer = nullptr;
	int	  _top = -1;
};
class ReplaceFuncs
{
public:
	struct FunctionCtx
	{
		string name;
		strvec params;
		string func_ctx;
	};

public:
	ReplaceFuncs() = default;
	~ReplaceFuncs() = default;

	bool had(cqstr name) const;
	const FunctionCtx& get_ctx(cqstr name) const;
	int add_function(cqstr func_src);
	int replace_func(string& expression, cqstr name);
	int replace_all(string& expression);

private:
	std::vector<FunctionCtx> functions;
};

template <typename T>
struct CalculateCore
{
public:
	struct FuncParam
	{
		void* _func_address = nullptr;
		uint32_t _param_num = -1;
	};
	struct ValueParam
	{
		char _value[cValueBufferSize] = { 0, 0 };
	};
	typedef T(*func_param_0)();
	typedef T(*func_param_1)(T);
	typedef T(*func_param_2)(T, T);
	typedef T(*func_param_3)(T, T, T);
	typedef T(*func_param_4)(T, T, T, T);
	typedef T(*func_param_5)(T, T, T, T, T);
	typedef T(*func_param_6)(T, T, T, T, T, T);
	typedef T(*func_param_7)(T, T, T, T, T, T, T);
	typedef T(*func_param_8)(T, T, T, T, T, T, T, T);
	typedef T(*func_param_9)(T, T, T, T, T, T, T, T, T);
	typedef T(*func_param_x)(T, T, T, T, T, T, T, T, T, T);

public:
	int _priority = 0;
	char _symbol[32] = { '0', '0' };
	CType	 _ctype = _undefined;
	union
	{
		FuncParam  _func_param;
		ValueParam _value_param;
	};
	void* _array_address = nullptr;

public:
	CalculateCore() noexcept {};
	CalculateCore(int index, bool is_array)
		:_ctype(CType::_array)
	{
		*(T*)_value_param._value = (T)index;
		_priority = 100;
	}
	CalculateCore(const T& value, cqstr name = "") noexcept
	{
		set_value(value);
		strcpy(_symbol, name.c_str());
	}
	CalculateCore(void* func, cqstr name, int priority = 0, int param_num = 0) noexcept
	{
		_ctype = CType::_function;
		_func_param._func_address = func;
		_func_param._param_num = param_num;
		_priority = priority;
		strcpy(_symbol, name.c_str());
	}
	CalculateCore(T* values, int size)
	{
		_ctype = CType::_array;
		_array_address = malloc(sizeof(T) * size);
		memcpy(_array_address, values, sizeof(T) * size);
	}
	CalculateCore(char c) : _ctype(CType::_undefined)
	{
		if (c == '(') _ctype = CType::_left_brackets;
		else if (c == ')') _ctype = CType::_right_brackets;
	}
	~CalculateCore()
	{
		if (_array_address != nullptr)
		{
			free(_array_address);
			_array_address = nullptr;
		}
	}

	T get_value() const
	{
		return *(T*)_value_param._value;
	}
	int get_value(T& value) const noexcept
	{
		if (_ctype != CType::_number) return -1;
		value = *(T*)_value_param._value;
		return 0;
	}
	int set_value(const T& value) noexcept
	{
		*(T*)_value_param._value = value;
		_ctype = CType::_number;
		return 0;
	}

	void set_func(void* dst_func, int param_num)
	{
		if (dst_func == nullptr) return;
		_func_param._func_address = dst_func;
		_func_param._param_num = param_num;
	}

	template <typename FuncType>
	FuncType cvt_func() const
	{
		return (FuncType)_func_param._func_address;
	}

	T calc_0() const { return cvt_func<func_param_0>()(); }
	T calc_1(T v) const { return cvt_func<func_param_1>()(v); }
	T calc_2(T v, T v1) const { return cvt_func<func_param_2>()(v, v1); }
	T calc_3(T v, T v1, T v2) const { return cvt_func<func_param_3>()(v, v1, v2); }
	T calc_4(T v, T v1, T v2, T v3) const { return cvt_func<func_param_4>()(v, v1, v2, v3); }
	T calc_5(T v, T v1, T v2, T v3, T v4) const { return cvt_func<func_param_5>()(v, v1, v2, v3, v4); }
	T calc_6(T v, T v1, T v2, T v3, T v4, T v5) const { return cvt_func<func_param_6>()(v, v1, v2, v3, v4, v5); }
	T calc_7(T v, T v1, T v2, T v3, T v4, T v5, T v6) const
	{
		return cvt_func<func_param_7>()(v, v1, v2, v3, v4, v5, v6);
	}
	T calc_8(T v, T v1, T v2, T v3, T v4, T v5, T v6, T v7) const
	{
		return cvt_func<func_param_8>()(v, v1, v2, v3, v4, v5, v6, v7);
	}
	T calc_9(T v, T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8) const
	{
		return cvt_func<func_param_9>()(v, v1, v2, v3, v4, v5, v6, v7, v8);
	}
	T calc_x(T v, T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9) const
	{
		return cvt_func<func_param_x>()(v, v1, v2, v3, v4, v5, v6, v7, v8, v9);
	}
};

template <typename T>
using CalcVec = std::vector<CalculateCore<T>>;
template <typename T>
void show_calc_vec(const CalcVec<T>& vec)
{
	for (auto item : vec)
	{
		if (item._ctype == CType::_number)
		{
			const char* fmt = "%d ";
			if (get_c_type<T>() == get_c_type<double>() ||
				get_c_type<T>() == get_c_type<float>())
			{
				fmt = "%.02f ";
			}
			T value;
			item.get_value(value);
			printf(fmt, value);
		}
		else if (item._ctype == CType::_function)
		{
			printf("%s ", item._symbol);
		}
		else if (item._ctype == CType::_array)
		{
			printf("array:%d ", (int)item.get_value());
		}
		else if (item._ctype == CType::_left_brackets)
		{
			printf("(");
		}
		else if (item._ctype == CType::_right_brackets)
		{
			printf(")");
		}
	}
	puts("");
}

std::string strfmt_va(const char* fmt, va_list vl);
std::string strfmt(const char* fmt, ...);
bool is_number(cqstr str);
stringset double_func_operator();
strvec slice_by(cqstr src, char flag);
strvec fliter(const strvec& src, std::function<bool(string)> func);
std::vector<std::pair<size_t, size_t>> find_brackets_pos(cqstr str, bool throw_error = true);
strvec parse_name_params(cqstr expression);
strvec parse_array_params(cqstr expression);
string remove_prefix_suffix_c(cqstr src, char c);
strvec expression_slice(cqstr expression);

enum class FuncType
{
	u8, i8, u16, i16, u32, i32, u64, i64, f, d
};
template<typename T> FuncType get_func_type()
{
	if (get_c_type<T>() == get_c_type<double>()) return FuncType::d;
	else if (get_c_type<T>() == get_c_type<float>()) return FuncType::f;
	else if (get_c_type<T>() == get_c_type<int64_t>()) return FuncType::i64;
	else if (get_c_type<T>() == get_c_type<uint64_t>()) return FuncType::u64;
	else if (get_c_type<T>() == get_c_type<int32_t>()) return FuncType::i32;
	else if (get_c_type<T>() == get_c_type<uint32_t>()) return FuncType::u32;
	else if (get_c_type<T>() == get_c_type<int16_t>()) return FuncType::i16;
	else if (get_c_type<T>() == get_c_type<uint16_t>()) return FuncType::u16;
	else if (get_c_type<T>() == get_c_type<int8_t>()) return FuncType::i8;
	else if (get_c_type<T>() == get_c_type<uint8_t>()) return FuncType::u8;
	else return FuncType::d;
}

struct StaticFuncInfor
{
	string name;
	int param_num = 0;
	int priority = 0;
	void* context = nullptr;
};
bool had_function(cqstr name);
int get_function(FuncType type, cqstr name, StaticFuncInfor& infor);

template<typename T> class GlobalArray
{
public:
	GlobalArray() = default;
	~GlobalArray()
	{
		clear();
	}

	void clear()
	{
		for (int i = 0; i < top; i++)
		{
			free(_array_ctx[top]);
			_array_ctx[top] = nullptr;
		}
		top = -1;
	}
	bool empty() const { return top == -1; }
	uint32_t size() const { return top + 1; }

	void add_array(const T* src, uint32_t size)
	{
		++top;
		T* tmp_buffer = (T*)malloc(size * sizeof(T) + 1);
		if (tmp_buffer != nullptr)
		{
			memcpy(tmp_buffer, src, size * sizeof(T));
			_array_ctx[top] = tmp_buffer;
			_array_size[top] = size;
		}
	}

	bool exist(cqstr name) const
	{
		for (uint32_t i = 0; i < size(); i++)
		{
			if (_array_name[i] == name)
				return true;
		}
		return false;
	}

	int add_array(cqstr expression)
	{
		static const std::regex _array_reg(R"(array\s+\w+\s?=\s?\{\s?[\w\W]+?\})");
		std::smatch sm;
		string src = expression;
		if (!std::regex_search(src, sm, _array_reg))
		{
			return -1;
		}
		if (sm[0].str() != expression)
		{
			return -2;
		}

		auto qpos = expression.find('=');
		string name = expression.substr(6, qpos - 6);
		name = remove_prefix_suffix_c(name, ' ');
		if (exist(name))
		{
			return -4;
		}

		auto pos = expression.find("{");
		if (pos == string::npos)
		{
			return -3;
		}

		string array_ctx = expression.substr(pos + 1, expression.size() - pos - 2);
		auto slice_ret = slice_by(array_ctx, ',');
		for (auto& sr : slice_ret)
		{
			sr = remove_prefix_suffix_c(sr, ' ');
		}

		std::vector<T> valvec;
		for (auto& sr : slice_ret)
		{
			if (get_c_type<T>() == get_c_type<float>() || get_c_type<T>() == get_c_type<double>())
			{
				valvec.push_back((T)atof(sr.c_str()));
			}
			else
			{
#ifdef _WIN32
				valvec.push_back((T)_atoi64(sr.c_str()));
#else
				valvec.push_back((T)strtoll(sr.c_str(), nullptr, 10));
#endif
			}
		}

		add_array(valvec.data(), (uint32_t)valvec.size());
		_array_name[top] = name;

		return 0;
	}

	int get_index(cqstr name)
	{
		for (uint32_t i = 0; i < size(); i++)
		{
			if (_array_name[i] == name)
				return (int)i;
		}
		return -1;
	}

public:
	T* _array_ctx[cMaxArrayNum];
	uint32_t _array_size[cMaxArrayNum] = { 0 };
	string _array_name[cMaxArrayNum];
	int top = -1;
};

template <typename T>
CalcVec<T> parse_expression(cqstr expression,
	const stringset& accept_var = {},
	GlobalArray<T>* _array = nullptr)
{
	CalcVec<T> result;
	strvec sliceret;

	try
	{
		auto brackets = find_brackets_pos(expression);
		sliceret = expression_slice(expression);
	} catch (const string& e)
	{
		printf("parse expression failed %s", e.c_str());
		throw e;
	}
	sliceret = fliter(sliceret, [](cqstr str) { return str.length() > 0; });
	auto dfo = double_func_operator();

	for (auto item : sliceret)
	{
		if (is_number(item))
		{
			if (get_c_type<T>() == get_c_type<float>() ||
				get_c_type<T>() == get_c_type<double>())
			{
				result.push_back(CalculateCore<T>((T)atof(item.c_str())));
			}
			else
			{
				result.push_back(CalculateCore<T>((T)atoi(item.c_str())));
			}
		}
		else if (had_function(item))
		{
			StaticFuncInfor infor;
			get_function(get_func_type<T>(), item, infor);
			result.push_back(CalculateCore<T>(infor.context, infor.name, infor.priority, infor.param_num));
		}
		else if (item == "(")
		{
			result.push_back(CalculateCore<T>('('));
		}
		else if (item == ")")
		{
			result.push_back(CalculateCore<T>(')'));
		}
		else
		{
			strvec name_param;
			try
			{
				name_param = parse_name_params(item);
			} catch (const std::string& e)
			{
				throw e;
			}

			if (name_param.size() > 0)
			{
				if (had_function(name_param[0]))
				{
					StaticFuncInfor infor;
					int getret = get_function(get_func_type<T>(), name_param[0], infor);
					if (infor.param_num == 0 && name_param.size() == 1)
					{
						result.push_back(CalculateCore<T>(infor.context, infor.name,
							infor.priority, infor.param_num));
					}
					else if (infor.param_num == 1 && name_param.size() == 2)
					{
						result.push_back(CalculateCore<T>('('));
						try
						{
							auto tmp_expression = parse_expression<T>(name_param[1], accept_var, _array);

							result.push_back(CalculateCore<T>(infor.context,
								infor.name, infor.priority, infor.param_num));

							result.push_back(CalculateCore<T>('('));
							result.insert(result.end(), tmp_expression.begin(), tmp_expression.end());
							result.push_back(CalculateCore<T>(')'));
						} catch (const string& e)
						{
							throw e;
						}
						result.push_back(CalculateCore<T>(')'));
						continue;
					}
					else if (infor.param_num == 2 && name_param.size() == 3)
					{
						result.push_back(CalculateCore<T>('('));
						try
						{
							auto tmp_expression = parse_expression<T>(name_param[1], accept_var, _array);
							result.push_back(CalculateCore<T>('('));
							result.insert(result.end(), tmp_expression.begin(), tmp_expression.end());
							result.push_back(CalculateCore<T>(')'));
						} catch (const string& e)
						{
							throw e;
						}

						result.push_back(CalculateCore<T>(infor.context,
							infor.name, infor.priority, infor.param_num));

						try
						{
							auto tmp_expression = parse_expression<T>(name_param[2], accept_var, _array);
							result.push_back(CalculateCore<T>('('));
							result.insert(result.end(), tmp_expression.begin(), tmp_expression.end());
							result.push_back(CalculateCore<T>(')'));
						} catch (const string& e)
						{
							throw e;
						}
						result.push_back(CalculateCore<T>(')'));

						continue;
					}
					else if (infor.param_num + 1 == name_param.size())
					{
						result.push_back(CalculateCore<T>('('));

						for (size_t i = 0; i < infor.param_num; i++)
						{
							try
							{
								auto tmp_expression = parse_expression<T>(name_param[i + 1], accept_var, _array);
								result.push_back(CalculateCore<T>('('));
								result.insert(result.end(), tmp_expression.begin(), tmp_expression.end());
								result.push_back(CalculateCore<T>(')'));

							} catch (const string& e)
							{
								throw e;
							}
						}

						result.push_back(CalculateCore<T>(infor.context,
							infor.name, infor.priority, infor.param_num));

						result.push_back(CalculateCore<T>(')'));

						continue;
					}
				}
			}

			if (_array != nullptr)
			{
				strvec array_param = parse_array_params(item);
				if (array_param.size() == 2 && _array->exist(array_param[0]))
				{
					try
					{
						auto tmp_expression = parse_expression<T>(array_param[1], accept_var, _array);

						result.push_back(CalculateCore<T>('('));
						result.push_back(CalculateCore<T>((int)_array->get_index(array_param[0]), true));
						result.push_back(CalculateCore<T>('('));
						result.insert(result.end(), tmp_expression.begin(), tmp_expression.end());
						result.push_back(CalculateCore<T>(')'));
						result.push_back(CalculateCore<T>(')'));

						continue;
					} catch (const std::string& e)
					{
						throw e;
					}
				}
			}

			if (accept_var.find(item) != accept_var.end())
			{
				result.push_back(CalculateCore<T>((T)0, item));
				continue;
			}

			throw strfmt("parse_expression illegal format>%s<", item.c_str());
		}
	}

	return result;
}

template <typename T>
CalcVec<T> infix2suffix(const CalcVec<T>& src)
{
	std::vector<CalculateCore<T>> ret;
	std::list<CalculateCore<T>> list1;
	std::list<CalculateCore<T>> list2;
	auto list_top = [](std::list<CalculateCore<T>>& list)->CalculateCore<T> {
		return *(--list.end());
	};

	int i = 0;
	for (auto& item : src)
	{
		if (item._ctype == CType::_function && item._func_param._param_num == 2)
		{
			while (true)
			{
				if (list1.empty() || list_top(list1)._ctype == CType::_left_brackets)
				{
					list1.push_back(item);
					break;
				}
				else if (item._priority > list_top(list1)._priority)
				{
					list1.push_back(item);
					break;
				}
				else
				{
					auto c = list_top(list1);
					list1.pop_back();
					list2.push_back(c);
				}
			}
		}
		else if (item._ctype == CType::_function && item._func_param._param_num == 0
			|| item._ctype == CType::_number)
		{
			list2.push_back(item);
		}
		else if (item._ctype == CType::_function || item._ctype == CType::_array)
		{
			list1.push_back(item);
		}
		else
		{
			if (item._ctype == CType::_left_brackets)
			{
				list1.push_back(item);
			}
			else
			{
				while (list_top(list1)._ctype != CType::_left_brackets)
				{
					auto c = list_top(list1);
					list1.pop_back();
					list2.push_back(c);
				}
				list1.pop_back();
			}
		}
	}

	while (!list1.empty())
	{
		auto c = list_top(list1);
		list2.push_back(c);
		list1.pop_back();
	}

	while (!list2.empty())
	{
		ret.push_back(list_top(list2));
		list2.pop_back();
	}

	std::reverse(ret.begin(), ret.end());

	return ret;
}

template <typename T>
T calculate(const CalcVec<T>& src, SimpleBufferStack& calc_stack, GlobalArray<T>* _array = nullptr)
{
	T param_array[cMaxParamsNum] = { 0 };
	T* ppa = param_array;

	using calc_func = std::function<T(CalculateCore<T>*)>;
	static calc_func calc_p0 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_0();
	};
	static calc_func calc_p1 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_1(ppa[0]);
	};
	static calc_func calc_p2 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_2(ppa[0], ppa[1]);
	};
	static calc_func calc_p3 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_3(ppa[0], ppa[1], ppa[2]);
	};
	static calc_func calc_p4 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_4(ppa[0], ppa[1], ppa[2], ppa[3]);
	};
	static calc_func calc_p5 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_5(ppa[0], ppa[1], ppa[2], ppa[3], ppa[4]);
	};
	static calc_func calc_p6 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_6(ppa[0], ppa[1], ppa[2], ppa[3], ppa[4], ppa[5]);
	};
	static calc_func calc_p7 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_7(ppa[0], ppa[1], ppa[2], ppa[3], ppa[4], ppa[5], ppa[6]);
	};
	static calc_func calc_p8 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_8(ppa[0], ppa[1], ppa[2], ppa[3], ppa[4], ppa[5], ppa[6], ppa[7]);
	};
	static calc_func calc_p9 = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_9(ppa[0], ppa[1], ppa[2], ppa[3], ppa[4], ppa[5], ppa[6], ppa[7], ppa[8]);
	};
	static calc_func calc_px = [ppa](CalculateCore<T>* _core)->T {
		return _core->calc_x(ppa[0], ppa[1], ppa[2], ppa[3], ppa[4], ppa[5], ppa[6], ppa[7], ppa[8], ppa[8]);
	};

	static calc_func funcarray[cMaxParamsNum] = {
		calc_p0, calc_p1, calc_p2, calc_p3,
		calc_p4, calc_p5, calc_p6, calc_p7,
		calc_p8, calc_p9, calc_px
	};

	calc_stack.clear();

	const CalculateCore<T>* calcore = src.data();

	for (size_t i = 0; i < src.size(); i++, calcore++)
	{
		CType ctype = calcore->_ctype;

		if (ctype == CType::_number)
		{
			calc_stack.push((void*)calcore->_value_param._value);
		}
		else if (ctype == CType::_function)
		{
			for (int n = (int)calcore->_func_param._param_num - 1; n >= 0; --n)
			{
				ppa[n] = *(T*)calc_stack.top();
				calc_stack.pop();
			}
			T v = funcarray[calcore->_func_param._param_num]((CalculateCore<T>*)calcore);
			calc_stack.push(&v);
		}
		else if (ctype == CType::_array)
		{
			T vv = *(T*)calc_stack.top();
			int index = (int)vv;
			int array_id = (int)calcore->get_value();
			calc_stack.pop();
			calc_stack.push(_array->_array_ctx[array_id] + index);
		}
	}

	T* pvalue = (T*)calc_stack.top();
	return *pvalue;
}

template <typename T> class ExpressionCalculater
{
public:
	ExpressionCalculater(cqstr expression = "")
	{
		set_expression(expression);
	}
	~ExpressionCalculater() = default;

	void set_expression(cqstr exp) { _expression = exp; };
	string get_expression() const { return _expression; }

	void add_variale(cqstr name)
	{
		_variable.insert(name);
	}
	void add_variales(const strvec& name)
	{
		for (auto n : name)
		{
			add_variale(n);
		}
	}

	int init(bool show_process = false, bool throw_error = false)
	{
		_array.clear();
		string exp = _expression;
		size_t pos = 0;
		while ((pos = exp.find('\n')) != string::npos)
		{
			exp.replace(pos, 1, "");
		}
		strvec slice_ret = slice_by(exp, ';');

		ReplaceFuncs functions;
		for (size_t i = 0; i < slice_ret.size() - 1; i++)
		{
			if (slice_ret[i].substr(0, 5) == "array")
			{
				int ret = _array.add_array(slice_ret[i]);
				if (ret != 0)
				{
					printf("illegal array expression:%s \n", slice_ret[i].c_str());
				}
			}
			else if (slice_ret[i].substr(0, 4) == "func")
			{
				int ret = functions.add_function(slice_ret[i]);
				if (ret != 0)
				{
					printf("illegal func expression:%d %s\n", ret, slice_ret[i].c_str());
				}
			}
		}

		try
		{
			functions.replace_all(slice_ret[slice_ret.size() - 1]);
			_context = parse_expression(slice_ret[slice_ret.size() - 1], _variable, &_array);
			if (show_process)
			{
				printf("infix :");
				show_calc_vec<T>(_context);
			}
			_context = infix2suffix(_context);
			if (show_process)
			{
				printf("suffix:");
				show_calc_vec<T>(_context);
			}
		} catch (const string& e)
		{
			if (throw_error)
			{
				throw e;
			}
			else
			{
				printf("%s \n", e.c_str());
				return -1;
			}
		}

		return 0;
	}

	T calculate()
	{
		return ::calculate(_context, _stack, &_array);
	}

	void set_value(cqstr name, const T& value)
	{
		for (auto& c : _context)
			if (c._ctype == CType::_number && string(c._symbol) == name)
				c.set_value(value);
	}

private:
	string		_expression;
	CalcVec<T>	_context;
	GlobalArray<T>	_array;
	stringset	_variable;
	SimpleBufferStack _stack;
};

#endif
