#include "math_parser.h"
#include <random>

std::string strfmt_va(const char* fmt, va_list vl)
{
	std::vector<char> buffer(1024);
	vsnprintf(buffer.data(), buffer.size(), fmt, vl);
	return buffer.data();
}

std::string strfmt(const char* fmt, ...)
{
	va_list vl;
	va_start(vl, fmt);
	return strfmt_va(fmt, vl);
}

bool is_number(cqstr str)
{
	const std::regex _reg(R"(([+-][0-9]*\.?[0-9]+)|([0-9]*\.?[0-9]*))");
	std::smatch sm;
	if (!std::regex_match(str, sm, _reg)) return false;
	return sm[0].str() == str;
}

stringset double_func_operator()
{
	return { "<<", ">>", "+", "-", "*", "/", "%", "^",
		">", "<", "==", "<=", ">=", "&&", "||", "&", "|" };
}

strvec slice_by(cqstr src, char flag)
{
	strvec result;
	if (src.length() == 0) return result;

	auto pos = src.find(flag);
	if (pos == string::npos)
	{
		result.push_back(src);
	}
	else
	{
		result.push_back(src.substr(0, pos));
		auto tmp = slice_by(src.substr(pos + 1), flag);
		result.insert(result.end(), tmp.begin(), tmp.end());
	}

	return result;
}

strvec fliter(const strvec& src, std::function<bool(string)> func)
{
	strvec result;
	for (auto item : src)
	{
		if (func(item))
		{
			result.push_back(item);
		}
	}
	return result;
}

std::vector<std::pair<size_t, size_t>> find_brackets_pos(cqstr str, bool throw_error)
{
	std::vector<std::pair<size_t, size_t>> result;
	int left_count = 0;
	int left_beg_pos = -1;

	for (int i = 0; i < (int)str.size(); ++i)
	{
		if (str[i] == '(' && left_count == 0)
		{
			left_count++;
			left_beg_pos = i;
		}
		else if (str[i] == '(')
		{
			left_count++;
		}
		else if (str[i] == ')' && left_count <= 0)
		{
			if (throw_error)
			{
				throw strfmt("find_brackets_pos right illegal format");
			}
		}
		else if (str[i] == ')' && left_count > 1)
		{
			left_count--;
		}
		else if (str[i] == ')' && left_count == 1)
		{
			result.push_back(std::make_pair(left_beg_pos, i));
			left_count = 0;
			left_beg_pos = -1;
		}
	}

	if (left_count != 0 && throw_error)
	{
		throw strfmt("find_brackets_pos left illegal format");
	}

	return result;
}

strvec parse_name_params(cqstr expression)
{
	strvec result;
	const std::regex name_regex(R"([\w~]+)");
	const std::regex params_all(R"(\(([\w\W]+,?)*?\))");

	std::smatch name_sm;
	std::smatch params_sm;

	if (!std::regex_search(expression, name_sm, name_regex))
	{
		return result;
	}
	auto name = name_sm[0].str();

	if (expression.find(name) != 0)
	{
		return result;
	}
	result.push_back(name);

	string params_str = name_sm.suffix().str();
	if (!std::regex_search(params_str, params_sm, params_all))
	{
		return result;
	}

	if (params_sm[0].str() != params_str || params_sm[0].str() == "()")
	{
		return result;
	}
	params_str = params_sm[0].str();
	params_str = params_str.substr(1, params_str.size() - 2);

	std::vector<std::pair<size_t, size_t>> brackets;
	auto in_brackets = [&](int _pos) {
		for (auto br : brackets)
		{
			if (br.first < _pos && _pos < br.second)
			{
				return true;
			}
		}
		return false;
	};

	try
	{
		brackets = find_brackets_pos(params_str);
	} catch (const string&)
	{
		return result;
	}

	if (brackets.size() == 0)
	{
		auto _tmp = slice_by(params_str, ',');
		result.insert(result.end(), _tmp.begin(), _tmp.end());

		for (auto& r : result)
		{
			r = remove_prefix_suffix_c(r, ' ');
			if (r.size() == 0) throw strfmt("%s had void param", expression.c_str());
		}

		return result;
	}

	std::vector<size_t> points;
	size_t pos = 0;
	while ((pos = params_str.find(',', pos)) != string::npos)
	{
		if (!in_brackets((int)pos))
		{
			points.push_back(pos);
		}
		pos++;
	}

	if (points.size() == 0)
	{
		result.push_back(params_str);
	}
	else if (points.size() == 1)
	{
		result.push_back(params_str.substr(0, points[0]));
		result.push_back(params_str.substr(points[0] + 1));
	}
	else
	{
		for (auto iter = points.begin(); iter != points.end(); iter++)
		{
			auto push_str = [&] {
				size_t beg = *(iter - 1) + 1;
				size_t length = *iter - beg;
				string tmp_str = params_str.substr(beg, length);
				result.push_back(tmp_str);
			};
			if (iter == points.begin())
			{
				result.push_back(params_str.substr(0, *iter));
			}
			else if (iter + 1 == points.end())
			{
				push_str();
				result.push_back(params_str.substr(*iter + 1));
			}
			else
			{
				push_str();
			}
		}
	}

	for (auto& r : result)
	{
		r = remove_prefix_suffix_c(r, ' ');
		if (r.size() == 0) throw strfmt("%s had void param", expression.c_str());
	}

	return result;
}

strvec parse_array_params(cqstr expression)
{
	strvec result;
	std::regex all_reg(R"(\w+\[[\w\W]+?\])");
	std::smatch sm;

	string src = expression;
	if (!std::regex_search(src, sm, all_reg))
	{
		return result;
	}

	auto pos = expression.find('[');
	result.push_back(expression.substr(0, pos));
	result.push_back(expression.substr(pos + 1, expression.size() - pos - 2));

	return result;
}

string remove_prefix_suffix_c(cqstr src, char c)
{
	string result = src;
	while (result.size() > 0 && result[result.size() - 1] == c)
	{
		result.pop_back();
	}
	while (result.size() > 0 && result[0] == c)
	{
		result = result.substr(1);
	}
	return result;
}

strvec expression_slice(cqstr expression)
{
	strvec result;
	string str = expression;
	static const std::regex _reg(R"([\w]+\([\w\W]*?\))");
	static const std::regex _reg2(R"(\w+\[[\w\W]+?\])");
	std::smatch sm, sm2;
	strvec func_defs;
	strvec func_defs2;

	constexpr const char* replace_str = "_parse_replace_atgq";
	constexpr const char* replace_str2 = "_parse_replace_khvba";
	auto c2str = [](char c) {
		string result;
		result.push_back(c);
		return result;
	};

	while (std::regex_search(str, sm2, _reg2))
	{
		string _ctx = sm2[0].str();
		auto pos = str.find(_ctx);
		str.replace(pos, _ctx.size(), replace_str2);
		func_defs2.push_back(_ctx);
	}

	while (std::regex_search(str, sm, _reg))
	{
		string _ctx = sm[0].str();
		_ctx = str.substr(str.find(_ctx));

		auto pos = find_brackets_pos(_ctx, false);
		if (pos.size() > 0)
		{
			string _def = _ctx.substr(0, pos[0].second + 1);
			func_defs.push_back(_def);
			str.replace(str.find(_def), _def.size(), replace_str);
		}
	}

	stringset apartflags = double_func_operator();
	apartflags.insert("(");
	apartflags.insert(")");

	auto find_flag = [apartflags](cqstr c) ->size_t {
		auto iter = --apartflags.end();
		while (true)
		{
			if (c.substr(0, iter->size()) == *iter)
			{
				return iter->size();
			}
			if (iter == apartflags.begin())
			{
				break;
			}
			iter--;
		}
		return (size_t)0;
	};

	std::vector<std::pair<size_t, size_t>> flags;
	size_t pos = 0;
	for (size_t i = 0; i < str.size();)
	{
		if ((pos = find_flag(str.substr(i))) > 0)
		{
			flags.push_back(std::make_pair(i, pos));
			i += pos;
		}
		else
			i++;
	}

	if (flags.size() > 1)
	{
		for (size_t i = 0; i < flags.size() - 1; i++)
		{
			if (flags[i].first + flags[i].second == flags[i + 1].first
				&& (str.substr(flags[i].first, flags[i].second) != "(" &&
					str.substr(flags[i].first, flags[i].second) != ")"))
				throw strfmt("symbol closed >%s< ", str.substr(flags[i].first, flags[i].second + 1).c_str());
		}
	}

	if (flags.size() == 0)
	{
		result.push_back(remove_prefix_suffix_c(str, ' '));
	}
	else if (flags.size() == 1)
	{
		result.push_back(remove_prefix_suffix_c(str.substr(0, flags[0].first), ' '));
		result.push_back(str.substr(flags[0].first, flags[0].second));
		result.push_back(remove_prefix_suffix_c(str.substr(flags[0].first + flags[0].second), ' '));
	}
	else
	{
		for (auto iter = flags.begin(); iter != flags.end(); iter++)
		{
			auto push_back_iter = [&] {
				auto begpos = (iter - 1)->first + (iter - 1)->second;
				size_t length = iter->first - begpos;
				auto tmp_str = remove_prefix_suffix_c(str.substr(begpos, length), ' ');
				if (tmp_str.size() > 0) result.push_back(tmp_str);
				result.push_back(str.substr(iter->first, iter->second));
			};

			if (iter == flags.begin())
			{
				auto tmp_str = remove_prefix_suffix_c(str.substr(0, iter->first), ' ');
				if (tmp_str.size() > 0) result.push_back(tmp_str);
				result.push_back(str.substr(iter->first, iter->second));
			}
			else if (iter + 1 == flags.end())
			{
				push_back_iter();
				auto tmp_str = remove_prefix_suffix_c(str.substr(iter->first + iter->second), ' ');
				if (tmp_str.size() > 0) result.push_back(tmp_str);
			}
			else
			{
				push_back_iter();
			}
		}
	}

	int rindex = 0;
	for (auto& item : result)
	{
		if (item == replace_str)
		{
			item = func_defs[rindex++];
		}
	}

	rindex = 0;
	for (auto& item : result)
	{
		size_t fpos = 0;
		while ((fpos = item.find(replace_str2)) != string::npos)
		{
			item.replace(fpos, strlen(replace_str2), func_defs2[rindex++]);
		}
	}

	return result;
}

#define DefMathFuncP1(name) static T name(T v) { return (T)::name((double)v); }


template <typename T>
struct Functions
{
	static T fn(T v)
	{
		return (T)-(double)v;
	}
	static T fabs(T v)
	{
		return (T)((double)v < 0 ? -(double)v : (double)v);
	}
	static T n(T v)
	{
		return (T)-(int64_t)v;
	}
	static T abs(T v)
	{
		return (T)((int64_t)v < 0 ? -(int64_t)v : (int64_t)v);
	}
	static T neg(T v)
	{
		return v == 0 ? (T)1 : (T)0;
	}
	static T inc(T v)
	{
		return v + 1;
	}
	static T dec(T v)
	{
		return v + 1;
	}
	static T printi(T v)
	{
		printf("%s", strfmt("value :%lld \n", (int64_t)v).c_str());
		return v;
	}
	static T printui(T v)
	{
		printf("%s", strfmt("value :%llu \n", (uint64_t)v).c_str());
		return v;
	}

	DefMathFuncP1(sin);
	DefMathFuncP1(cos);
	DefMathFuncP1(tan);
	DefMathFuncP1(log);
	DefMathFuncP1(sqrt);

	static T random(T min, T max, T interval)
	{
		if (interval == 0) return (T)0;
		static std::default_random_engine random((uint32_t)time(NULL));
		T count = (max - min) / interval;
		if (count <= 1) return (T)min;
		static std::uniform_int_distribution<uint32_t> dis(0, (uint32_t)count);
		return interval * dis(random) + min;
	}
	static T printd(T v, T precision)
	{
		string format = strfmt("value :%c.0%df \n", '%', (int)precision);
		printf(format.c_str(), v);
		return v;
	}
	static T add(T v1, T v2) { return v1 + v2; }
	static T sub(T v1, T v2) { return v1 - v2; }
	static T mul(T v1, T v2) { return v1 * v2; }
	static T div(T v1, T v2) { return v1 / v2; }
	static T rem(T v1, T v2)
	{
		return (T)((uint64_t)v1 % (uint64_t)v2);
	}
	static T move_left(T v1, T v2)
	{
		return (T)((uint64_t)v1 << (uint64_t)v2);
	}
	static T move_right(T v1, T v2)
	{
		return (T)((uint64_t)v1 << (uint64_t)v2);
	}
	static T power(T v1, T v2)
	{
		return (T)::pow(v1, v2);
	}
	static T greater(T v1, T v2)
	{
		return v1 > v2;
	}
	static T smaller(T v1, T v2)
	{
		return v1 < v2;
	}
	static T equal(T v1, T v2)
	{
		return v1 == v2;
	}
	static T le(T v1, T v2)
	{
		return v1 <= v2;
	}
	static T ge(T v1, T v2)
	{
		return v1 >= v2;
	}
	static T logicand(T v1, T v2)
	{
		return v1 && v2;
	}
	static T logicor(T v1, T v2)
	{
		return v1 || v2;
	}
	static T _and(T v1, T v2)
	{
		return (T)((uint64_t)v1 & (uint64_t)v2);
	}
	static T _or(T v1, T v2)
	{
		return (T)((uint64_t)v1 | (uint64_t)v2);
	}
	static T max(T v1, T v2)
	{
		return v1 > v2 ? v1 : v2;
	}
	static T min(T v1, T v2)
	{
		return v1 < v2 ? v1 : v2;
	}
	static T avg2(T v1, T v2)
	{
		return (v1 + v2) / 2;
	}


	static T ter(T v1, T v2, T v3)
	{
		return v1 ? v2 : v3;
	}
	static T in_range(T v1, T v2, T v3)
	{
		return v1 >= v2 && v1 <= v3;
	}
	static T avg3(T v1, T v2, T v3)
	{
		return (v1 + v2 + v3) / 3;
	}

	static T rect_pos(T v1, T v2, T v3, T v4, T v5)
	{
		return v4 * (v1 * v3 + v2) + v5;
	}
};

template<typename T> struct FuncInfor
{
	FuncInfor() {}
	FuncInfor(cqstr n, int pn, int p, void* ctx)
		:name(n), param_num(pn), priority(p), context(ctx)
	{}
	~FuncInfor() = default;
	string name = "";
	int param_num = 0;
	int priority = 0;
	void* context = nullptr;
	static std::map<string, FuncInfor> infors;
	static FuncInfor* get_infor(cqstr name)
	{
		if (infors.find(name) == infors.end()) return nullptr;
		return &infors[name];
	}
	StaticFuncInfor tosfi()
	{
		StaticFuncInfor ret;
		ret.context = context;
		ret.name = name;
		ret.param_num = param_num;
		ret.priority = priority;
		return ret;
	}
};
template<typename T>
std::map<string, FuncInfor<T>> FuncInfor<T>::infors = {
	{ "neg", FuncInfor("neg", 1, 100, (void*)&Functions<T>::neg) },
	{ "dec", FuncInfor("dec", 1, 100, (void*)&Functions<T>::dec) },
	{ "inc", FuncInfor("inc", 1, 100, (void*)&Functions<T>::inc) },
	{ "printi", FuncInfor("printi", 1, 100, (void*)&Functions<T>::printi) },
	{ "printui", FuncInfor("printui", 1, 100, (void*)&Functions<T>::printui) },
	{ "sin", FuncInfor("sin", 1, 100, (void*)&Functions<T>::sin) },
	{ "cos", FuncInfor("cos", 1, 100, (void*)&Functions<T>::cos) },
	{ "tan", FuncInfor("tan", 1, 100, (void*)&Functions<T>::tan) },
	{ "log", FuncInfor("log", 1, 100, (void*)&Functions<T>::log) },
	{ "fabs", FuncInfor("fabs", 1, 100, (void*)&Functions<T>::fabs) },
	{ "fn", FuncInfor("fn", 1, 100, (void*)&Functions<T>::fn) },
	{ "abs", FuncInfor("abs", 1, 100, (void*)&Functions<T>::abs) },
	{ "n", FuncInfor("n", 1, 100, (void*)&Functions<T>::n) },

	{ "printd", FuncInfor("printd", 2, 100, (void*)&Functions<T>::printd) },
	{ "+", FuncInfor("+", 2, 4, (void*)&Functions<T>::add) },
	{ "add", FuncInfor("add", 2, 4, (void*)&Functions<T>::add) },
	{ "-", FuncInfor("-", 2, 4, (void*)&Functions<T>::sub) },
	{ "sub", FuncInfor("sub", 2, 4, (void*)&Functions<T>::sub) },
	{ "*", FuncInfor("*", 2, 5, (void*)&Functions<T>::mul) },
	{ "mul", FuncInfor("mul", 2, 5, (void*)&Functions<T>::mul) },
	{ "/", FuncInfor("/", 2, 5, (void*)&Functions<T>::div) },
	{ "div", FuncInfor("div", 2, 5, (void*)&Functions<T>::div) },
	{ "%", FuncInfor("%", 2, 5, (void*)&Functions<T>::rem) },
	{ "rem", FuncInfor("rem", 2, 5, (void*)&Functions<T>::rem) },
	{ "<<", FuncInfor("<<", 2, 5, (void*)&Functions<T>::move_left) },
	{ "left", FuncInfor("left", 2, 5, (void*)&Functions<T>::move_left) },
	{ ">>", FuncInfor(">>", 2, 5, (void*)&Functions<T>::move_right) },
	{ "right", FuncInfor("right", 2, 5, (void*)&Functions<T>::move_right) },
	{ "^", FuncInfor("^", 2, 5, (void*)&Functions<T>::power) },
	{ "pow", FuncInfor("pow", 2, 5, (void*)&Functions<T>::power) },
	{ ">", FuncInfor(">", 2, 3, (void*)&Functions<T>::greater) },
	{ "g", FuncInfor("g", 2, 3, (void*)&Functions<T>::greater) },
	{ "<", FuncInfor("<", 2, 3, (void*)&Functions<T>::smaller) },
	{ "s", FuncInfor("g", 2, 3, (void*)&Functions<T>::smaller) },
	{ "==", FuncInfor("==", 2, 3, (void*)&Functions<T>::equal) },
	{ "eq", FuncInfor("eq", 2, 3, (void*)&Functions<T>::equal) },
	{ "<=", FuncInfor("<=", 2, 3, (void*)&Functions<T>::le) },
	{ "le", FuncInfor("le", 2, 3, (void*)&Functions<T>::le) },
	{ ">=", FuncInfor(">=", 2, 3, (void*)&Functions<T>::ge) },
	{ "ge", FuncInfor("ge", 2, 3, (void*)&Functions<T>::ge) },
	{ "&&", FuncInfor("&&", 2, 5, (void*)&Functions<T>::logicand) },
	{ "la", FuncInfor("la", 2, 5, (void*)&Functions<T>::logicand) },
	{ "||", FuncInfor("||", 2, 5, (void*)&Functions<T>::logicor) },
	{ "lo", FuncInfor("lo", 2, 5, (void*)&Functions<T>::logicor) },
	{ "&", FuncInfor("&", 2, 5, (void*)&Functions<T>::_and) },
	{ "and", FuncInfor("and", 2, 5, (void*)&Functions<T>::_and) },
	{ "|", FuncInfor("|", 2, 5, (void*)&Functions<T>::_or) },
	{ "or", FuncInfor("or", 2, 5, (void*)&Functions<T>::_or) },
	{ "max", FuncInfor("max", 2, 100, (void*)&Functions<T>::max) },
	{ "min", FuncInfor("min", 2, 100, (void*)&Functions<T>::min) },
	{ "avg2", FuncInfor("avg2", 2, 100, (void*)&Functions<T>::avg2) },

	{ "random", FuncInfor("random", 3, 100, (void*)&Functions<T>::random) },
	{ "in", FuncInfor("in", 3, 100, (void*)&Functions<T>::in_range) },
	{ "ter", FuncInfor("ter", 3, 100, (void*)&Functions<T>::ter) },
	{ "avg3", FuncInfor("avg3", 2, 100, (void*)&Functions<T>::avg3) },

	{ "rect_pos", FuncInfor("rect_pos", 5, 100, (void*)&Functions<T>::rect_pos) },
};

bool had_function(cqstr name)
{
	return FuncInfor<int>::infors.find(name) != FuncInfor<int>::infors.end();
}

int get_function(FuncType type, cqstr name, StaticFuncInfor& infor)
{
	if (!had_function(name)) return -1;

	switch (type)
	{
	case FuncType::u8:
		infor = FuncInfor<uint8_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::i8:
		infor = FuncInfor<int8_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::u16:
		infor = FuncInfor<uint16_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::i16:
		infor = FuncInfor<int16_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::u32:
		infor = FuncInfor<uint32_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::i32:
		infor = FuncInfor<int32_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::u64:
		infor = FuncInfor<uint64_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::i64:
		infor = FuncInfor<int64_t>::get_infor(name)->tosfi();
		return 0;
	case FuncType::f:
		infor = FuncInfor<float>::get_infor(name)->tosfi();
		return 212;
	case FuncType::d:
		infor = FuncInfor<double>::get_infor(name)->tosfi();
		return 0;
	default:
		return -3;
	}
}

SimpleBufferStack::SimpleBufferStack(int element_size)
	:_element_size(element_size)
{
	_buffer = malloc(max_length * element_size);
}

SimpleBufferStack::~SimpleBufferStack()
{
	if (_buffer)
	{
		free(_buffer);
		_buffer = 0;
	}
}

void SimpleBufferStack::push(void* value)
{
	memcpy((uint8_t*)_buffer + _element_size * ++_top, value, _element_size);
}

void SimpleBufferStack::pop()
{
	if (_top == -1) return;
	_top--;
}

bool SimpleBufferStack::empty() const
{
	return _top == -1;
}

void* SimpleBufferStack::top() const
{
	return (uint8_t*)_buffer + _element_size * _top;
}

void SimpleBufferStack::clear()
{
	_top = -1;
}

bool ReplaceFuncs::had(cqstr name) const
{
	for (const auto& item : functions)
	{
		if (item.name == name) return true;
	}
	return false;
}

const ReplaceFuncs::FunctionCtx& ReplaceFuncs::get_ctx(cqstr name) const
{
	for (auto& item : functions)
	{
		if (item.name == name) return item;
	}
	return functions[0];
}

int ReplaceFuncs::add_function(cqstr func_src)
{
	static const std::regex
		_func_regex(R"(func\s+\w+\s*=\s*\(\s*(\w+\s*,\s*)*\w+\s*\)->\s*\{\s*return\s*[\S\s]+?\})");
	std::smatch sm_func;
	string srcstr = func_src;
	if (!std::regex_search(srcstr, sm_func, _func_regex))
	{
		return -1;
	}

	static const std::regex
		_name_regex(R"(func\s+\w+\s*=)");
	std::smatch sm_name;
	if (!std::regex_search(srcstr, sm_name, _name_regex))
	{
		return -2;
	}
	FunctionCtx ctx;

	ctx.name = sm_name[0].str();
	ctx.name = ctx.name.substr(5, ctx.name.size() - 6);
	ctx.name = remove_prefix_suffix_c(ctx.name, ' ');
	if (had(ctx.name))
	{
		return -9;
	}
	if (had_function(ctx.name))
	{
		return -10;
	}

	static const std::regex
		_param_regex(R"(\(\s*(\w+\s*,\s*)*\w+\s*\))");
	std::smatch sm_param;
	if (!std::regex_search(srcstr, sm_param, _param_regex))
	{
		return -3;
	}
	string param_src = sm_param[0].str();
	param_src = param_src.substr(1, param_src.size() - 2);
	ctx.params = slice_by(param_src, ',');
	for (auto& p : ctx.params)
	{
		p = remove_prefix_suffix_c(p, ' ');
	}

	for (size_t j = 0; j < ctx.params.size(); j++)
	{
		for (size_t i = 0; i < ctx.params.size(); i++)
		{
			if (i != j && ctx.params[i] == ctx.params[j])
				return -5;
		}
	}

	static const std::regex
		_ctx_regex(R"(return\s+[\S\s]+?\})");
	std::smatch sm_ctx;
	if (!std::regex_search(srcstr, sm_ctx, _ctx_regex))
	{
		return -6;
	}
	ctx.func_ctx = sm_ctx[0].str();
	ctx.func_ctx = ctx.func_ctx.substr(7, ctx.func_ctx.size() - 8);
	ctx.func_ctx = remove_prefix_suffix_c(ctx.func_ctx, ' ');

	for (uint32_t i = 0; i < ctx.params.size(); i++)
	{
		string from = ctx.params[i];
		string to = strfmt("%s_%u", ctx.name.c_str(), i);
		size_t pos = 0;
		while ((pos = ctx.func_ctx.find(from)) != string::npos)
		{
			ctx.func_ctx.replace(pos, from.size(), to);
		}
	}

	replace_all(ctx.func_ctx);
	functions.push_back(ctx);

	return 0;
}

int ReplaceFuncs::replace_func(string& expression, cqstr name)
{
	if (!had(name)) return -1;
	auto& ctx = get_ctx(name);
	string regex_str = ctx.name + R"(\()";
	for (size_t i = 0; i < ctx.params.size() - 1; i++)
	{
		regex_str += R"(\s*\w+\s*,\s*)";
	}
	regex_str += R"(\s*\w+\s*\))";

	std::regex param_regex(regex_str);
	std::smatch sm;
	string str_src = expression;
	while (std::regex_search(str_src, sm, param_regex))
	{
		string replace_src = sm[0].str();

		string param_src = replace_src.substr(name.size() + 1, replace_src.size() - name.size() - 2);

		auto svec = slice_by(param_src, ',');
		for (auto& p : svec)
		{
			p = remove_prefix_suffix_c(p, ' ');
		}

		string replace_dest = "(" + ctx.func_ctx + ")";
		for (uint32_t i = 0; i < (uint32_t)ctx.params.size(); i++)
		{
			string src = strfmt("%s_%u", name.c_str(), i);
			string dst = svec[i];
			size_t pos = 0;
			while ((pos = replace_dest.find(src)) != string::npos)
			{
				replace_dest.replace(pos, src.size(), dst);
			}
		}

		size_t pos = expression.find(replace_src);
		expression.replace(pos, replace_src.size(), replace_dest);

		str_src = sm.suffix().str();
	}

	return 0;
}

int ReplaceFuncs::replace_all(string& expression)
{
	for (auto& item : functions)
	{
		replace_func(expression, item.name);
	}
	return 0;
}
